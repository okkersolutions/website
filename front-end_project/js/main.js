var app = angular.module('SensorApp', ["chart.js"]);

app.config(['$httpProvider', function($httpProvider) {

    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
}
]);

app.controller('AreaCtrl', function ($scope, $http) {
    $scope.list;

    $scope.selected = null;
    $scope.selectArea = function (area) {
        $scope.selected = area;
    };
    $http.get('./js/area.json').then(function (response, err)
    {
        if (err) console.log(err);
        console.log(response.data);
        $scope.list = response.data.area;
    });

});

app.controller('LocationCtrl', function ($scope, $http) {
    $scope.location_list;


    $http.get('http://localhost:80/api/api/location').then(function (response, err) {
        if (err) console.log(err);
        console.log(response.data);
        $scope.location_list = response.data;
        $scope.selected_location = $scope.location_list[0];
        console.log( $scope.selected_location);
    });


    $scope.select_location = function (area) {
        $scope.selected_location = area;
    };
});

app.controller("LineCtrl", function ($scope, $http) {

    $scope.labels = ["09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00"];
    $scope.date = new Date;
    $scope.series = ['Temperatuur in graden celsius op ' + $scope.date.toTimeString()];
    $scope.temp_data;

    $http.get('./js/sensor_data.json').then(function (response, err) {
        if (err) console.log(err);
        console.log(response);
        $scope.temp_data = response.data.temp_data;
    });

    $scope.onClick = function (points, evt) {
        console.log(points, evt);
    };
});

app.controller("BarCtrl", function ($scope) {
    $scope.bar_labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    $scope.bar_series = ['Series A', 'Series B'];

    $scope.bar_data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
    ];
});
              
